/* CNgine TK replacemencet */
#include "headers/sim.h"

SimView *gview;

SimView *TileViewCmd(int viewclass)
{
    SimView *view = (SimView*)CN_malloc(sizeof(SimView));
    view->flags = 0;
    view->tkwin = 0;
    view->interp = 0;
    view->class = viewclass;
    if(viewclass == Editor_Class)
    {
        //CreateEventHandler
        //TileViewEventProc

        //CreateCommand
        //DoEditorCmd
    } else {
        //CreateEventHandler
        //TileViewEventProc

        //CreateCommand
        //DoMapCmd
    }


    if (viewclass == Editor_Class) {
        InitNewView(view, "MicropolisEditor", Editor_Class, EDITOR_W, EDITOR_H);
        DoNewEditor(view);
    } else {
        InitNewView(view, "MicropolisMap", Map_Class, MAP_W, MAP_H);
        DoNewMap(view);
    }
    view->visible = 1;
    switch(view->class) {
    case Editor_Class:
        break;
    case Map_Class:
        view->invalid = 1;
        view->update = 1;
        break;
    }
    return view;
}

