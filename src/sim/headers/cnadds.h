//////////////////////////////
// CNgine version functions //
//////////////////////////////

// Pixmap loader
//cn_pmap.c
CN_Image *customLoadPixmap(char *path);
extern SimView *gview;

//cn_main.c
int CN_MainLoop(void);

// CNgine UI replacements
// cn_uir.c

void UIStartMicropolis();
void UINewGame();
void UISetMessage(char *message);
void UISetSpeed(int speed);
void UISetDemand(double r, double c, double i);
void UISetOptions(
    CN_bool autobudget,
    CN_bool autogoto,
    CN_bool autobulldoze,
    CN_bool disasters,
    CN_bool sound,
    CN_bool animation,
    CN_bool messages,
    CN_bool notices);
void UISetDate(char *date, int month, int year);
void UISetGameLevel(int level);
void UIReallyStartGame();
void UISplashMode();
void WithdrawAll();
void InitSplashes();
void UIInitGlobals();
void UIShowPicture(int id);
void DoGenerate();
void UIGenerateNewCity();
void DoNewCity(
    char *name,
    int level,
    int r,
    int tl,
    int ll,
    int cl,
    int ci);
void UIShowPicture(int id);
void UIShowPictureOn(int id);
void UISetFunds(char *funds);
void UIDidGenerateNewCity();
