/* g_setup.c
 *
 * Micropolis, Unix Version.  This game was released for the Unix platform
 * in or about 1990 and has been modified for inclusion in the One Laptop
 * Per Child program.  Copyright (C) 1989 - 2007 Electronic Arts Inc.  If
 * you need assistance with this program, you may contact:
 *   http://wiki.laptop.org/go/Micropolis  or email  micropolis@laptop.org.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  You should have received a
 * copy of the GNU General Public License along with this program.  If
 * not, see <http://www.gnu.org/licenses/>.
 * 
 *             ADDITIONAL TERMS per GNU GPL Section 7
 * 
 * No trademark or publicity rights are granted.  This license does NOT
 * give you any right, title or interest in the trademark SimCity or any
 * other Electronic Arts trademark.  You may not distribute any
 * modification of this program using the trademark SimCity or claim any
 * affliation or association with Electronic Arts Inc. or its employees.
 * 
 * Any propagation or conveyance of this program must include this
 * copyright notice and these terms.
 * 
 * If you convey this program (or any modifications of it) and assume
 * contractual liability for the program to recipients of it, you agree
 * to indemnify Electronic Arts for any liability that those contractual
 * assumptions impose on Electronic Arts.
 * 
 * You may not misrepresent the origins of this program; modified
 * versions of the program must be marked as such and not identified as
 * the original program.
 * 
 * This disclaimer supplements the one included in the General Public
 * License.  TO THE FULLEST EXTENT PERMISSIBLE UNDER APPLICABLE LAW, THIS
 * PROGRAM IS PROVIDED TO YOU "AS IS," WITH ALL FAULTS, WITHOUT WARRANTY
 * OF ANY KIND, AND YOUR USE IS AT YOUR SOLE RISK.  THE ENTIRE RISK OF
 * SATISFACTORY QUALITY AND PERFORMANCE RESIDES WITH YOU.  ELECTRONIC ARTS
 * DISCLAIMS ANY AND ALL EXPRESS, IMPLIED OR STATUTORY WARRANTIES,
 * INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, NONINFRINGEMENT OF THIRD PARTY
 * RIGHTS, AND WARRANTIES (IF ANY) ARISING FROM A COURSE OF DEALING,
 * USAGE, OR TRADE PRACTICE.  ELECTRONIC ARTS DOES NOT WARRANT AGAINST
 * INTERFERENCE WITH YOUR ENJOYMENT OF THE PROGRAM; THAT THE PROGRAM WILL
 * MEET YOUR REQUIREMENTS; THAT OPERATION OF THE PROGRAM WILL BE
 * UNINTERRUPTED OR ERROR-FREE, OR THAT THE PROGRAM WILL BE COMPATIBLE
 * WITH THIRD PARTY SOFTWARE OR THAT ANY ERRORS IN THE PROGRAM WILL BE
 * CORRECTED.  NO ORAL OR WRITTEN ADVICE PROVIDED BY ELECTRONIC ARTS OR
 * ANY AUTHORIZED REPRESENTATIVE SHALL CREATE A WARRANTY.  SOME
 * JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF OR LIMITATIONS ON IMPLIED
 * WARRANTIES OR THE LIMITATIONS ON THE APPLICABLE STATUTORY RIGHTS OF A
 * CONSUMER, SO SOME OR ALL OF THE ABOVE EXCLUSIONS AND LIMITATIONS MAY
 * NOT APPLY TO YOU.
 */
#include "headers/sim.h"


#define SIM_SMTILE	385
#define SIM_BWTILE	386
#define SIM_GSMTILE	388
#define SIM_LGTILE	544


#define gray25_width 16
#define gray25_height 16
static unsigned char gray25_bits[] = {
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
  0x77, 0x77,
  0xdd, 0xdd,
};


#define gray50_width 16
#define gray50_height 16
static unsigned char gray50_bits[] = {
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
  0x55, 0x55,
  0xaa, 0xaa,
};


#define gray75_width 16
#define gray75_height 16
static unsigned char gray75_bits[] = {
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
  0x88, 0x88,
  0x22, 0x22,
};

#define vert_width 16
#define vert_height 16
static unsigned char vert_bits[] = {
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
  0xaa, 0xaa,
};


#define horiz_width 16
#define horiz_height 16
static unsigned char horiz_bits[] = {
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
  0xff, 0xff,
  0x00, 0x00,
};


#define diag_width 16
#define diag_height 16
static unsigned char diag_bits[] = {
  0x55, 0x55, 
  0xee, 0xee, 
  0x55, 0x55, 
  0xba, 0xbb,
  0x55, 0x55, 
  0xee, 0xee, 
  0x55, 0x55, 
  0xba, 0xbb,
  0x55, 0x55, 
  0xee, 0xee,
  0x55, 0x55, 
  0xba, 0xbb,
  0x55, 0x55, 
  0xee, 0xee, 
  0x55, 0x55, 
  0xba, 0xbb,
};

Ptr MickGetHexa(short theID)
{
  Handle theData;

  theData = GetResource("hexa", theID);
  return ((Ptr)*theData);
}


CN_Image **GetObjectXpms(XDisplay *xd, int id, int frames)
{
  int i;
  char name[256];
  CN_Image **image = CN_malloc(sizeof(CN_Image*) * frames);
  for (i = 0; i < frames; i++) {
    sprintf(name, "%s/images/obj%d-%d.xpm", HomeDir, id, i);
    image[i] = customLoadPixmap(name);
  }
  return (image);
}


void GetPixmaps(XDisplay *xd)
{
/*  if (xd->gray25_stipple == None) {
    xd->gray25_stipple =
      XCreatePixmapFromBitmapData(xd->dpy, xd->root,
				  gray25_bits, gray25_width, gray25_height,
				  1, 0, 1);
    xd->gray50_stipple =
      XCreatePixmapFromBitmapData(xd->dpy, xd->root,
				  gray50_bits, gray50_width, gray50_height,
				  1, 0, 1);
    xd->gray75_stipple =
      XCreatePixmapFromBitmapData(xd->dpy, xd->root,
				  gray75_bits, gray75_width, gray75_height,
				  1, 0, 1);
    xd->vert_stipple =
      XCreatePixmapFromBitmapData(xd->dpy, xd->root,
				  vert_bits, vert_width, vert_height,
				  1, 0, 1);
    xd->horiz_stipple =
      XCreatePixmapFromBitmapData(xd->dpy, xd->root,
				  horiz_bits, horiz_width, horiz_height,
				  1, 0, 1);
    xd->diag_stipple =
      XCreatePixmapFromBitmapData(xd->dpy, xd->root,
				  diag_bits, diag_width, diag_height,
				  1, 0, 1);
                                  }*/

  if (xd->objects == NULL) {
    CN_Image ***image;

    xd->objects = image = (CN_Image ***)CN_malloc(OBJN * sizeof (CN_Image *));

    image[0] = NULL; /* no object number 0 */
    image[TRA] = GetObjectXpms(xd, TRA, 5);
    image[COP] = GetObjectXpms(xd, COP, 8);
    image[AIR] = GetObjectXpms(xd, AIR, 11);
    image[SHI] = GetObjectXpms(xd, SHI, 8);
    image[GOD] = GetObjectXpms(xd, GOD, 16);
    image[TOR] = GetObjectXpms(xd, TOR, 3);
    image[EXP] = GetObjectXpms(xd, EXP, 6);
    image[BUS] = GetObjectXpms(xd, BUS, 4);
  }
}


void GetViewTiles(SimView *view)
{
  char name[256];


  // Load bix_tile_pixmap
  if (view->x->big_tile_pixmap == NULL) {
      sprintf(name, "%s/images/%s", HomeDir, "tiles.xpm" );
      view->x->big_tile_pixmap = customLoadPixmap(name);
  }

  if(view->bigtiles == NULL)
  {
      int x,y;
      view->bigtiles =
          malloc(sizeof(char) *
                 CN_Image_GetWidth(view->x->big_tile_pixmap) *
                 CN_Image_GetHeight(view->x->big_tile_pixmap));
      for(y = 0; y < CN_Image_GetHeight(view->x->big_tile_pixmap); y++) {
          for(x = 0; x < CN_Image_GetWidth(view->x->big_tile_pixmap); x++) {
              view->bigtiles[y*x+x]
                  = CN_Image_GetPixel(view->x->big_tile_pixmap, x, y);
          }
      }
  }

  if (view->x->small_tile_image == NULL) {
      sprintf(name, "%s/images/%s", HomeDir, "tilessm.xpm");
      view->x->small_tile_image = customLoadPixmap(name);
  }

  if(view->smalltiles == NULL)
  {
      int x,y;
      view->smalltiles =
          malloc(sizeof(char) *
                 CN_Image_GetWidth(view->x->small_tile_image) *
                 CN_Image_GetHeight(view->x->small_tile_image));
      for(y = 0; y < CN_Image_GetHeight(view->x->small_tile_image); y++) {
          for(x = 0; x < CN_Image_GetWidth(view->x->small_tile_image); x++) {
              view->smalltiles[y*x+x]
                  = CN_Image_GetPixel(view->x->small_tile_image, x, y);
          }
      }
  }
  
/*  if (view->class == Map_Class) {
      int x, y, b, tile;
      unsigned char *from, *to;
      int pixelBytes = view->pixel_bytes;
      int rowBytes = CN_Image_GetWidth(view->x->small_tile_image);
      if (pixelBytes == 0) {
	/* handle the case of monochrome display (8 bit map) 
	pixelBytes = 1;
      }
      /* from is 4 pixels wide per 3 pixel wide tile 
      switch (pixelBytes) {
      case 1:
	for (tile = 0; tile < TILE_COUNT; tile++) {
	  for (y = 0; y < 3; y++) {
	    for (x = 0; x < 4; x++) {
	      *to++ = *from++;
	    }
	  }
	  for (x = 0; x < 4; x++) {
	    *to++ = 0;
	  }
	}
	break;

      case 2:
	for (tile = 0; tile < TILE_COUNT; tile++) {
	  for (y = 0; y < 3; y++) {
	    for (x = 0; x < 4; x++) {
	      *to++ = *from++;
	      *to++ = *from++;
	    }
	  }
	  for (x = 0; x < 4; x++) {
	    *to++ = 0;
	    *to++ = 0;
	  }
	}
	break;

      case 3:
      case 4:
	for (tile = 0; tile < TILE_COUNT; tile++) {
	  for (y = 0; y < 3; y++) {
	    for (x = 0; x < 4; x++) {
	      *to++ = *from++;
	      *to++ = *from++;
	      *to++ = *from++;
	      if (pixelBytes == 4) {
		*to++ = *from++;
	      }
	    }
	  }
	  for (x = 0; x < 4; x++) {
	    *to++ = 0;
	    *to++ = 0;
	    *to++ = 0;
	    if (pixelBytes == 4) {
	      *to++ = 0;
	    }
	  }
	}
	break;

      default:
	assert(0); /* Undefined depth 
	break;


      }
  }*/
}
