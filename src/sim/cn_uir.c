#include "headers/sim.h"
#include <string.h>

enum MicropolisState {
    MS_Uninitialized,
    MS_Splash,
    MS_Scenario,
    MS_Play
};


// Global Vars
char UICurrentMessage[256];
CN_bool AutoBudget, AutoGoto, AutoBulldoze, Disasters, Sound;
char CurrentDate[256];
int UniqueID;
enum MicropolisState State;
int OldBudget;
int BudgetRoadFund;
int BudgetFireFund;
int BudgetPoliceFund;
int BudgetTaxRate;
int DemandRes;
int DemandCom;
int DemandInd;
int Priority;
int Time;
int ShapePies;
int SoundServers;
//set AudioChannels {mode edit fancy warning intercom}
int BudgetTimeout;
int BudgetTimer;
int BudgetTimerActive;
int BudgetsVisible;
int EvaluationsVisible;
int SplashScreenDelay;
int Scenario;
int Chatting;
char *ChatServer;
int ChatSocket;
int ChatConnection;
char *NickName;
char *UserName;
char *ServerName;
char *RealName;
char *ChannelName;
char *LocalHostName;
char *SaveCityWin;
//set MapHistory {}
int MapHistoryNum;
int HelpLoaded;
int QueryX;
int QueryY;
int FreeVotes;
int ShowingPicture;
int MaxLines;
int ShrinkLines;
char *fundsMessage;
//set ShowingParms {}
//set VoteNames {UseThisMap Ask Zone}
//set VotesForUseThisMap {}
//set VotesForAsk {}
//set VotesForZone {}
//set VotesForBudget {}
//set CurrentDate {}

//set HeadWindows {}
//set EditorWindows {}
//set MapWindows {}
//set GraphWindows {}
//set BudgetWindows {}
//set EvaluationWindows {}
//set SplashWindows {}
//set ScenarioWindows {}
//set FileWindows {}
//set AskWindows {}
//set PlayerWindows {}
//set NoticeWindows {}
//set HelpWindows {}
//set FrobWindows {}

int HeadPanelWidth;
int HeadPanelHeight;

int MapPanelWidth;
int MapPanelHeight;

int NoticePanelWidth;
int NoticePanelHeight;

int SplashPanelWidth;
int SplashPanelHeight;

int ScenarioPanelWidth;
int ScenarioPanelHeight;

char *SugarURI;
char *SugarNickName;
int SugarShared;
int SugarActivated;
//set SugarBuddies {}


void UIInitGlobals()
{
    UniqueID = 0;
    State = MS_Uninitialized;
    CityName = "Micropolis";
    OldBudget = 0;
    OldBudget = 0;
    BudgetRoadFund = 0;
    BudgetFireFund = 0;
    BudgetPoliceFund = 0;
    BudgetTaxRate = 0;
    DemandRes = 0;
    DemandCom = 0;
    DemandInd = 0;
    Priority = 2;
    Time = 3;
    AutoGoto = 1;
    AutoBudget = 1;
    Disasters = 1;
    AutoBulldoze = 1;
    Sound = 1;
    DoAnimation = 1;
    DoMessages = 1;
    DoNotices = 1;
    ShapePies = 1;
//set SoundServers {}
//set AudioChannels {mode edit fancy warning intercom}
    BudgetTimeout = 30;
    BudgetTimer = 0;
    BudgetTimerActive = 0;
    BudgetsVisible = 0;
    EvaluationsVisible = 0;
    SplashScreenDelay = 5000;
    Scenario = -1;
    Chatting = 0;
    ChatServer = "localhost";
    ChatSocket = 6667;
//set ChatConnection {}
    NickName = "nickname";
    UserName = "username";
    ServerName = "servername";
    RealName = "realname";
    ChannelName = "#Micropolis";
    LocalHostName = "[exec hostname]";
    SaveCityWin = "";
//set MapHistory {}
    MapHistoryNum = -1;
    HelpLoaded = 0;
    QueryX = 0;
    QueryY = 0;
    FreeVotes = 0;
    ShowingPicture = 300;
    MaxLines = 500;
    ShrinkLines = 250;
//set ShowingParms {}
//set VoteNames {UseThisMap Ask Zone}
/*set VotesForUseThisMap {}
set VotesForAsk {}
set VotesForZone {}
set VotesForBudget {}
set CurrentDate {}

set HeadWindows {}
set EditorWindows {}
set MapWindows {}
set GraphWindows {}
set BudgetWindows {}
set EvaluationWindows {}
set SplashWindows {}
set ScenarioWindows {}
set FileWindows {}
set AskWindows {}
set PlayerWindows {}
set NoticeWindows {}
set HelpWindows {}
set FrobWindows {}*/

    HeadPanelWidth = 360;
    HeadPanelHeight = 200;

    MapPanelWidth = 360;
    MapPanelHeight = 330;

    NoticePanelWidth = 360;
    NoticePanelHeight = 250;

    SplashPanelWidth = 1200;
    SplashPanelHeight = 900;

    ScenarioPanelWidth = 420;
    ScenarioPanelHeight = 440;

    SugarURI = "";
    SugarNickName = "";
    SugarShared = 0;
    SugarActivated = 0;
//set SugarBuddies {}
}


void UIStartMicropolis()
{
    InitGame();
    GameStarted();
}

void UINewGame()
{
    //global OldBudget
    //set OldBudget 0
    InitGame();
    EraseOverlay();
    //InitEditors
    //InitMaps
    //InitGraphs
    //update
    UpdateMaps();
}

char *speedMessages[] = {"Time pauses.",
                         "Time flows slow.",
                         "Time flows medium.",
                         "Time flows fast."};

void UISetSpeed(int speed)
{
    UISetMessage(speedMessages[speed]);
}

void UISetMessage(char *message)
{
    strcpy(UICurrentMessage, message);
}

void UISetDemand(double r, double c, double i)
{
    // TODO: this is a stub
    // RCI bars
/*   global HeadWindows DemandRes DemandCom DemandInd */

/*   set DemandRes $r */
/*   set DemandCom $c */
/*   set DemandInd $i */

/*   if {$r <= 0} then {set ry0 32} else {set ry0 24} */
/*   set ry1 [expr "$ry0 - $r"] */
/*   if {$c <= 0} then {set cy0 32} else {set cy0 24} */
/*   set cy1 [expr "$cy0 - $c"] */
/*   if {$i <= 0} then {set iy0 32} else {set iy0 24} */
/*   set iy1 [expr "$iy0 - $i"] */

/*   foreach win $HeadWindows { */
/*     set can [WindowLink $win.demand] */
/* #    $can coords r 8 $ry0 14 $ry1 */
/* #    $can coords c 17 $cy0 23 $cy1 */
/* #    $can coords i 26 $iy0 32 $iy1 */
/*     $can coords r 49 $ry0 55 $ry1 */
/*     $can coords c 58 $cy0 64 $cy1 */
/*     $can coords i 67 $iy0 73 $iy1 */
/*   } */
}



void UISetOptions(
    CN_bool autobudget,
    CN_bool autogoto,
    CN_bool autobulldoze,
    CN_bool disasters,
    CN_bool sound,
    CN_bool animation,
    CN_bool messages,
    CN_bool notices)
{
    AutoBudget = autobudget;
    AutoGoto = autogoto;
    AutoBulldoze = autobulldoze;
    Disasters = disasters;
    Sound = sound;
    DoAnimation = animation;
    DoMessages = messages;
    DoNotices = notices;
}

void UISetDate(char *date, int month, int year)
{
    strcpy(CurrentDate, date);
// Don't know about below???    
//      foreach win $HeadWindows {
//    [WindowLink $win.date] Set $month $year
//  }

}

void UISetGameLevel(int level)
{
    GameLevel = level;
//  foreach win $ScenarioWindows {
//    UpdateLevelSelection $win
//  }
}

void UIReallyStartGame()
{
    UISplashMode();
}

void UISplashMode()
{
    Pause();
}

void WithdrawAll()
{
    //TODO: stub
    //Withdraw all windows
}

void InitSplashes()
{
    //empty
}

void DoGenerate()
{
    UIGenerateNewCity();
}

void UIGenerateNewCity()
{
    if(GameLevel == -1) {
        GameLevel = 0;
    }
    DoNewCity(
        "NowHere", GameLevel, Rand(255), TreeLevel, LakeLevel, CurveLevel,
        CreateIsland);
}

void DoNewCity(
    char *name,
    int level,
    int r,
    int tl,
    int ll,
    int cl,
    int ci)
{
    Scenario = -1;
    TreeLevel = tl;
    LakeLevel = ll;
    CurveLevel = cl;
    CreateIsland = ci;
    if(r == 0) { // ?
        GenerateNewCity();
    } else {
        GenerateSomeCity(r);
    }
    CityName = name;
    GameLevel = level;
    UIShowPicture(48);
}

void UIShowPicture(int id)
{
    UIShowPictureOn(id);
}

void UIShowPictureOn(int id)
{
    if(DoNotices == 0) return;
    ShowingPicture = id;
}

void UISetFunds(char *funds)
{
    if(fundsMessage == NULL) {
        fundsMessage = malloc(sizeof(char)*256);
    }
    strcpy(fundsMessage, funds);
}

void UIDidGenerateNewCity()
{
//    Update();
}
