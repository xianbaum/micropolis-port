#include "cngine/cngine.h"
#include "headers/sim.h"
int CN_MainLoopo(void);

static CN_RGB *createRGB()
{
    CN_RGB *rgb = malloc(sizeof(CN_RGB)*256);
    rgb[COLOR_WHITE] = CN_RGB_Create(0xff, 0xff, 0xff);
    rgb[COLOR_YELLOW] = CN_RGB_Create(0xff, 0xff, 0x00);
    rgb[COLOR_ORANGE] = CN_RGB_Create(0xff, 0x7f, 0x00);
    rgb[COLOR_RED] = CN_RGB_Create(0xff, 0x00, 0x00);
    rgb[COLOR_DARKRED] = CN_RGB_Create(0xbf, 0x00, 0x00);
    rgb[COLOR_DARKBLUE] = CN_RGB_Create(0x00, 0x00, 0xe6);
    rgb[COLOR_LIGHTBLUE] = CN_RGB_Create(0x66, 0x66, 0xe6);
    rgb[COLOR_BROWN] = CN_RGB_Create(0xcc, 0x4c, 0x4c);
    rgb[COLOR_LIGHTGREEN] = CN_RGB_Create(0x00, 0xe6, 0x00);
    rgb[COLOR_DARKGREEN] = CN_RGB_Create(0x00, 0x7f, 0x00);
    rgb[COLOR_OLIVE] = CN_RGB_Create(0x99, 0x7f, 0x4c);
    rgb[COLOR_LIGHTBROWN] = CN_RGB_Create(0xcc, 0x7f, 0x66);
    rgb[COLOR_LIGHTGRAY] = CN_RGB_Create(0xbf, 0xbf, 0xbf);
    rgb[COLOR_MEDIUMGRAY] = CN_RGB_Create(0x7f, 0x7f, 0x7f);
    rgb[COLOR_DARKGRAY] = CN_RGB_Create(0x3f, 0x3f, 0x3f);
    rgb[COLOR_BLACK] = CN_RGB_Create(0, 0, 0);
    return rgb;
}

void CN_Init(int argc, char **argv)
{
    sim = MakeNewSim();
    sim_init();
    UIInitGlobals();
    SimView *view = TileViewCmd(Editor_Class);
    gview = view;

    CN_Options *opt = malloc(sizeof(CN_Options));
    opt->loggingLevel = CN_LOGGING_LEVEL_ALL;
    gview->w_width = opt->width = 640;
    opt->windowTitle = "Micropolis";
    gview->w_height = opt->height = 480;
    opt->mode = CN_MODE_WINDOWED;
    opt->mouseHidden = CN_true;
    CN_Backend_Init(opt);
    free(opt);
    
    CN_RGB *pal = createRGB();
    CN_RGB_Set(pal);


    UIStartMicropolis();

    sim_init_main(1, "micropolis");
    UIGenerateNewCity();
}

void CN_Deinit()
{
    CN_Backend_Deinit();
}


void CN_Frame(int dt)
{
    CN_Backend_Update();
    if (CN_IsKeyDown(CN_KEY_ESC)) CN_exit = CN_true;
    if(CN_IsKeyPressed(CN_KEY_D)) {
        gview->tool_mode = 1;
    }
    if(CN_IsKeyPressed(CN_KEY_G)) {
        UIGenerateNewCity();
    }
    //else CN_Sleep(10);
    gview->tool_mode = 1;
    gview->class = Editor_Class;
    TileAutoScrollProc(gview);
    if(gview->flags & VIEW_REDRAW_PENDING) {
        MemDrawBeegMapRect(gview, gview->pan_x,gview->pan_y,40,30);
    }
    DoUpdateMap(gview);
    //DoUpdateMap(gview);
    CN_Rect cursor;
    CN_GetMouseCoords(&(cursor.x), &(cursor.y));
    cursor.w = 8;
    cursor.h = 8;
    CN_Rect_DrawSolid(CN_GetScreen(), cursor, 1);


    CN_Flip();
    CN_Backend_FinalizeFrame();
}
