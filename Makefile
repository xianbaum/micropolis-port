all:
	cd src; make

clean:
	cd src; make clean

libretro:
	cd src; make libretro
